from haversine import haversine, Unit
import pandas as pd
from collections import namedtuple 

class Utils:
    def find_ind_start(self, arg):
        return arg.index("('")+2
    
    def find_ind_end(self, arg):
        return arg.index("')")
    
    def computeDistanceHarvesine(self, homeGPS, awayGPS):
        if homeGPS['latitude'] == 0 or homeGPS['longitude'] == 0 or awayGPS['latitude'] == 0 or awayGPS['longitude'] == 0:
            return 0
        homeDistance = (homeGPS['latitude'], homeGPS['longitude'])
        awayDistance = (awayGPS['latitude'], awayGPS['longitude'])
        distance = haversine(homeDistance, awayDistance)
        return distance
    
    def remove_duplicates_by_property(list_of_objects, property_name):
        seen = set()
        new_list = []
        for obj in list_of_objects:
            if obj[property_name] not in seen:
                seen.add(obj[property_name])
                new_list.append(obj)
        return new_list
    def get_team_data_by_league(data, league):
        teams = []
        for team in data:
            if team['doc']['nameLeague'] == league:
                teams.append(team)
        return teams
    def get_team_data_by_id(data, id):
        team_r = {}
        for team in data:
            if team['doc']['_id'] == id:
                team_r = team
        return team_r
    def get_position_pyth_exp_point(pyth,wpc,a,b):
        x_point = pyth.item()
        y_point = wpc.item()
        y_calc = a.item()*x_point + b.item()
        PythExpectPoint = namedtuple('PythExpectPoint', 'pyth_origin wpc_origin wpc_calc')
        pep = PythExpectPoint(pyth_origin=x_point, wpc_origin=y_point, wpc_calc=y_calc)
        return pep