import re
from datetime import datetime
import json
import unicodedata
from requests import get
from bs4 import BeautifulSoup as soup
from bs4.element import Tag
from optadatalib import opta_constants
from typing import Any

class SoccerStat:
    def __init__(self):
        pass

    # Get Tabs by ID
    def get_tabs_by_id(self,page,id) -> Tag:
        tabs = page.find_all("div", {"class": "tabs"})
        tabid: Tag = Tag(name="none")
        for tab in tabs:
            tabItem = tab.find_all("input", {"id" : re.compile(id)})
            if len(tabItem) > 0:
                tabid = tab
        return tabid

    # Match goals stats at half-time
    def build_half_time_goals_stats(self,url):
        page_goals_stats = soup(get(url).content, "html.parser")
        id = "_overunder_ht_tabone"
        tab = self.get_tabs_by_id(page=page_goals_stats, id=id)
        data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
        half_time_goals = []
        for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                    'id': '',
                    'data': {
                    'GP': 0,
                    'AVG': 0.0,
                    'plus_0_5': 0,
                    'plus_1_5': 0,
                    'plus_2_5': 0,
                    'plus_3_5': 0,
                    'plus_4_5': 0,
                    'plus_5_5': 0,
                    'BTS': 0,
                    'CS': 0,
                    'FTS': 0,
                    'WTN': 0,
                    'LTN': 0
                    }
                }
                nameTeam = unicodedata.normalize("NFKD", td_all[0].get_text().strip())
                GP = unicodedata.normalize("NFKD", td_all[1].get_text().strip())
                AVG = unicodedata.normalize("NFKD", td_all[2].get_text().strip())
                plus_0_5 = unicodedata.normalize("NFKD", td_all[3].get_text().strip())
                plus_0_5 = float(plus_0_5.rstrip(plus_0_5[-1]))/100
                plus_1_5 = unicodedata.normalize("NFKD", td_all[4].get_text().strip())
                plus_1_5 = float(plus_1_5.rstrip(plus_1_5[-1]))/100
                plus_2_5 = unicodedata.normalize("NFKD", td_all[5].get_text().strip())
                plus_2_5 = float(plus_2_5.rstrip(plus_2_5[-1]))/100
                plus_3_5 = unicodedata.normalize("NFKD", td_all[6].get_text().strip())
                plus_3_5 = float(plus_3_5.rstrip(plus_3_5[-1]))/100
                plus_4_5 = unicodedata.normalize("NFKD", td_all[7].get_text().strip())
                plus_4_5 = float(plus_4_5.rstrip(plus_4_5[-1]))/100
                plus_5_5 = unicodedata.normalize("NFKD", td_all[8].get_text().strip())
                plus_5_5 = float(plus_5_5.rstrip(plus_5_5[-1]))/100
                BTS = unicodedata.normalize("NFKD", td_all[9].get_text().strip())
                BTS = float(BTS.rstrip(BTS[-1]))/100
                CS = unicodedata.normalize("NFKD", td_all[10].get_text().strip())
                CS = float(CS.rstrip(CS[-1]))/100
                FTS = unicodedata.normalize("NFKD", td_all[11].get_text().strip())
                FTS = float(FTS.rstrip(FTS[-1]))/100
                WTN = unicodedata.normalize("NFKD", td_all[12].get_text().strip())
                WTN = float(WTN.rstrip(WTN[-1]))/100
                LTN = unicodedata.normalize("NFKD", td_all[13].get_text().strip())
                LTN = float(LTN.rstrip(LTN[-1]))/100
                team_data['id'] = nameTeam.rstrip()
                team_data['data']['GP'] = int(GP)
                team_data['data']['AVG'] = float(AVG)
                team_data['data']['plus_0_5'] = plus_0_5 
                team_data['data']['plus_1_5'] = plus_1_5
                team_data['data']['plus_2_5'] = plus_2_5
                team_data['data']['plus_3_5'] = plus_3_5
                team_data['data']['plus_4_5'] = plus_4_5
                team_data['data']['plus_5_5'] = plus_5_5
                team_data['data']['BTS'] = BTS
                team_data['data']['CS'] = CS
                team_data['data']['FTS'] = FTS
                team_data['data']['WTN'] = WTN
                team_data['data']['LTN'] = LTN  
                half_time_goals.append(team_data)
        return half_time_goals


    # Scored / conceded at half-time   
    def build_half_time_score_conceded(self,url,abbr):
        page_score_conceded = soup(get(url).content, "html.parser")
        id = r"_SCHT_1"
        tab = self.get_tabs_by_id(page=page_score_conceded, id=id)
        data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
        half_time_score_conceded = []
        for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                    'id': unicodedata.normalize("NFKD", td_all[6].get_text().strip()).rstrip(),
                    'data': {
                    'avg_scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text().strip())),
                    '0_scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text().strip())),
                    '1_scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text().strip())),
                    '2_scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text().strip())),
                    '3_scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[1].get_text().strip())),
                    '4_plus_scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[0].get_text().strip())),
                    'avg_conceded': self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text().strip())),
                    '0_conceded': self.normalizeNum(unicodedata.normalize("NFKD", td_all[8].get_text().strip())),
                    '1_conceded': self.normalizeNum(unicodedata.normalize("NFKD", td_all[9].get_text().strip())),
                    '2_conceded': self.normalizeNum(unicodedata.normalize("NFKD", td_all[10].get_text().strip())),
                    '3_conceded': self.normalizeNum(unicodedata.normalize("NFKD", td_all[11].get_text().strip())),
                    '4_plus_conceded': self.normalizeNum(unicodedata.normalize("NFKD", td_all[12].get_text().strip()))
                    }
                }
                half_time_score_conceded.append(team_data)
        return half_time_score_conceded
    
    # Record when leading and trailing at half-time
    def build_half_time_record_leading_trailing(self,url, id):
        page_record_leading_trailing = soup(get(url).content, "html.parser")
        tab = self.get_tabs_by_id(page=page_record_leading_trailing, id=id)
        data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
        half_time_record_leading_trailing = []
        for team_i_row in data:
                td_all = team_i_row.find_all("td")
                goals = self.getGoalsForAgainst(unicodedata.normalize("NFKD", td_all[6].get_text().strip()))
                team_data = {
                    'id': unicodedata.normalize("NFKD", td_all[0].get_text().strip()).rstrip(),
                    'data': {
                    'percentage': self.normalizePercent(unicodedata.normalize("NFKD", td_all[2].get_text().strip())),
                    'w': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text().strip())),
                    'd': self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text().strip())),
                    'l': self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text().strip())),
                    'pts': self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text().strip())),
                    'avg_pts': self.normalizeNum(unicodedata.normalize("NFKD", td_all[8].get_text().strip())),
                    'gf': goals['GF'],
                    'ga': goals['GA']
                    }
                }
                half_time_record_leading_trailing.append(team_data)
        return half_time_record_leading_trailing
    
    # Goals by half
    def build_half_time_goals_by(self,url):
        page_goals_by = soup(get(url).content, "html.parser")
        td: Any = self.getElementByTagAndText(page_goals_by, 'h2', 'Goals by half')
        data = td.parent.find('table',{"id": "btable"}).find_all('tr',{"class": "odd"})
        half_time_goals_by = []
        for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                    'id': unicodedata.normalize("NFKD", td_all[0].get_text().strip()).rstrip(),
                    'data': {
                    'scored': self.normalizeNum(unicodedata.normalize("NFKD", td_all[1].get_text().strip())),
                    'first_half': self.normalizePercent(unicodedata.normalize("NFKD", td_all[2].get_text().strip())),
                    'second_half': self.normalizePercent(unicodedata.normalize("NFKD", td_all[3].get_text().strip())),
                    'avg_minute': self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text().strip()))
                    }
                }
                half_time_goals_by.append(team_data)
                
        return half_time_goals_by
    # HT/FT home and HT/FT away
    def build_home_away_ht_ft(self,url,id):
        page_home_away_ht_ft = soup(get(url).content, "html.parser")
        td: Any = self.getElementByTagAndText(page_home_away_ht_ft, 'h2', id)
        data = td.parent.find('table',{"id": "btable"}).find_all('tr',{"class": "odd"})
        half_time_home_away_ht_ft = []
        for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                    'id': unicodedata.normalize("NFKD", td_all[1].get_text().strip()).rstrip(),
                    'data': {
                    'pts': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text().strip())),
                    'p': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text().strip())),
                    'w': self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text().strip())),
                    'd': self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text().strip())),
                    'l': self.normalizeNum(unicodedata.normalize("NFKD", td_all[6].get_text().strip())),
                    '1/1': self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text().strip())),
                    '1/x': self.normalizeNum(unicodedata.normalize("NFKD", td_all[8].get_text().strip())),
                    '1/2': self.normalizeNum(unicodedata.normalize("NFKD", td_all[9].get_text().strip())),
                    'x/1': self.normalizeNum(unicodedata.normalize("NFKD", td_all[10].get_text().strip())),
                    'x/x': self.normalizeNum(unicodedata.normalize("NFKD", td_all[11].get_text().strip())),
                    'x/2': self.normalizeNum(unicodedata.normalize("NFKD", td_all[12].get_text().strip())),
                    '2/1': self.normalizeNum(unicodedata.normalize("NFKD", td_all[13].get_text().strip())),
                    '2/x': self.normalizeNum(unicodedata.normalize("NFKD", td_all[14].get_text().strip())),
                    '2/2': self.normalizeNum(unicodedata.normalize("NFKD", td_all[15].get_text().strip()))
                    }
                }
                half_time_home_away_ht_ft.append(team_data)
                
        return half_time_home_away_ht_ft
    # Wide Table 
    def buildWideTable(self,url):
        # Wide Table
        page_widetable = soup(get(url).content, "html.parser")
        data = page_widetable.find_all("table", {"id": "btable"})[0].find_all('tbody')[0].find_all('tr',{"class": "odd"} )
        wide_table = []
        for team_i_row in data:
            td_all = team_i_row.find_all("td")
            wide_table_team = {
                'id': unicodedata.normalize("NFKD", td_all[1].get_text().strip()).strip(),
                'data': {
                    'total': {'GP':0, 'W': 0, 'D': 0, 'L': 0, 'GF': 0, 'GA': 0, 'GD': 0, 'Pts': 0, 'PPG': 0, 'WPC_T': 0, 'PYTH_T': 0},
                    'home': {'Wh': 0, 'Dh': 0,'Lh': 0, 'GFh': 0,'GAh': 0},
                    'away': {'Wa': 0, 'Da': 0, 'La': 0, 'GFa': 0, 'GAa': 0}
                }
            }
            # Total
            wide_table_team['data']['total']['GP'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text()))
            wide_table_team['data']['total']['W'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text()))
            wide_table_team['data']['total']['D'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text()))
            wide_table_team['data']['total']['L'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text()))
            wide_table_team['data']['total']['GF'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[6].get_text()))
            wide_table_team['data']['total']['GA'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text()))
            wide_table_team['data']['total']['GD'] = td_all[8].get_text()
            wide_table_team['data']['total']['Pts'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[9].get_text()))
            wide_table_team['data']['total']['PPG'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[11].get_text()))
            WPC_T = wide_table_team['data']['total']['W']/wide_table_team['data']['total']['GP']
            PYTH_T = wide_table_team['data']['total']['GF']**2 /(wide_table_team['data']['total']['GF']**2 + wide_table_team['data']['total']['GA']**2)  
            wide_table_team['data']['total']['WPC_T'] = WPC_T
            wide_table_team['data']['total']['PYTH_T'] = PYTH_T

            # Home
            wide_table_team['data']['home']['Wh'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[13].get_text()))
            wide_table_team['data']['home']['Dh'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[14].get_text()))
            wide_table_team['data']['home']['Lh'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[15].get_text()))
            wide_table_team['data']['home']['GFh'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[16].get_text()))
            wide_table_team['data']['home']['GAh'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[17].get_text()))
            WPC_H = wide_table_team['data']['home']['Wh']/(wide_table_team['data']['home']['Wh'] + wide_table_team['data']['home']['Dh'] + wide_table_team['data']['home']['Lh'])
            PYTH_H = wide_table_team['data']['home']['GFh']**2 /(wide_table_team['data']['home']['GFh']**2 + wide_table_team['data']['home']['GAh']**2)  
            wide_table_team['data']['home']['WPC_H'] = WPC_H
            wide_table_team['data']['home']['PYTH_H'] = PYTH_H

            # Away
            wide_table_team['data']['away']['Wa'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[24].get_text()))
            wide_table_team['data']['away']['Da'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[25].get_text()))
            wide_table_team['data']['away']['La'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[26].get_text()))
            wide_table_team['data']['away']['GFa'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[27].get_text()))
            wide_table_team['data']['away']['GAa'] = self.normalizeNum(unicodedata.normalize("NFKD", td_all[28].get_text()))
            WPC_A = wide_table_team['data']['away']['Wa']/(wide_table_team['data']['away']['Wa'] + wide_table_team['data']['away']['Da'] + wide_table_team['data']['away']['La'])
            PYTH_A = wide_table_team['data']['away']['GFa']**2 /(wide_table_team['data']['away']['GFa']**2 + wide_table_team['data']['away']['GAa']**2)  
            wide_table_team['data']['away']['WPC_A'] = WPC_A
            wide_table_team['data']['away']['PYTH_A'] = PYTH_A

            
            wide_table.append(wide_table_team)
        return wide_table

    def getAllLeagues(self):
        page = soup(get(opta_constants.url_soccer_stat).content, "html.parser") # type: ignore
        leagues = page.find_all("td", {"align": "center", "valign": "middle",
        "style":"background-color:#ffffff;line-height:14px;"})
        leagues_infos = []
        title_regex = re.compile('<span title="(.*?)">')
        href_regex = re.compile('href="(.*?)"')
        for league in leagues:
            league_info = {}
            league_info['title'] = re.findall(title_regex, str(league))[0] 
            league_info['latest'] = "/".join([opta_constants.url_soccer_stat, re.findall(href_regex, str(league))[0]]) 
            league_info['widetable'] = league_info['latest'].replace('latest','widetable')
            league_info['trends'] = league_info['latest'].replace('latest','trends')
            league_info['formtable'] = league_info['latest'].replace('latest','formtable')
            league_info['halftime'] = league_info['latest'].replace('latest','halftime')
            # Table
            league_info['relative_form'] = league_info['latest'].replace('latest','table')+'&tid=re'
            league_info['relative_perf'] = league_info['latest'].replace('latest','table')+'&tid=rp'
            league_info['home_advantage'] = league_info['latest'].replace('latest','table')+'&tid=ha'
            league_info['points_win_lost'] = league_info['latest'].replace('latest','table')+'&tid=pw'
            league_info['current_streaks'] = league_info['latest'].replace('latest','table')+'&tid=g'
            league_info['goals_per_10'] = league_info['latest'].replace('latest','table')+'&tid=j'
            league_info['goals_per_15'] = league_info['latest'].replace('latest','table')+'&tid=k'
            league_info['over_under'] = league_info['latest'].replace('latest','table')+'&tid=c'
            league_info['lead_durations'] = league_info['latest'].replace('latest','table')+'&tid=t'
            league_info['goals_types'] = league_info['latest'].replace('latest','table')+'&tid=u'
            league_info['goals_ranges'] = league_info['latest'].replace('latest','table')+'&tid=9'
            league_info['abbr'] = league.text
            leagues_infos.append(league_info)
        # ADD-ONS
        leagues_add_ons = [
             {'title': 'Bresil', 'text':'BR','latest': 'https://www.soccerstats.com/latest.asp?league=brazil'},
             {'title': 'Chili', 'text':'CHI','latest': 'https://www.soccerstats.com/latest.asp?league=chile'},
             {'title': 'Colombia', 'text':'COL','latest': 'https://www.soccerstats.com/latest.asp?league=colombia'},
             {'title': 'Croatia', 'text':'CRO','latest': 'https://www.soccerstats.com/latest.asp?league=croatia'},
             {'title': 'Ecuador', 'text':'ECU','latest': 'https://www.soccerstats.com/latest.asp?league=ecuador'},
             {'title': 'Espagne2', 'text':'ES2','latest': 'https://www.soccerstats.com/latest.asp?league=spain2'},
             {'title': 'USA', 'text':'USA','latest': 'https://www.soccerstats.com/latest.asp?league=usa'},
             {'title': 'Ireland', 'text':'IRE','latest': 'https://www.soccerstats.com/latest.asp?league=ireland'},
             {'title': 'Israel', 'text':'ISR','latest': 'https://www.soccerstats.com/latest.asp?league=israel'},
             {'title': 'Northern Ireland', 'text':'NOI','latest': 'https://www.soccerstats.com/latest.asp?league=northernireland'},
             {'title': 'Mexico', 'text':'MEX','latest': 'https://www.soccerstats.com/latest.asp?league=mexico'},
             {'title': 'Paraguay', 'text':'PAR','latest': 'https://www.soccerstats.com/latest.asp?league=paraguay'},
             {'title': 'Wales', 'text':'WAL','latest': 'https://www.soccerstats.com/latest.asp?league=wales'},
             {'title': 'Czech Republic', 'text':'CZR','latest': 'https://www.soccerstats.com/latest.asp?league=czechrepublic'},
             {'title': 'Romania', 'text':'ROM','latest': 'https://www.soccerstats.com/latest.asp?league=romania'},
             {'title': 'Slovakia', 'text':'SLO','latest': 'https://www.soccerstats.com/latest.asp?league=slovakia'},
             {'title': 'Norway', 'text':'NOR','latest': 'https://www.soccerstats.com/latest.asp?league=norway'}]
        for add_on in leagues_add_ons:
            league_info = {}
            league_info['title'] = add_on['title'] 
            league_info['latest'] = add_on['latest'] 
            league_info['widetable'] = league_info['latest'].replace('latest','widetable')
            league_info['trends'] = league_info['latest'].replace('latest','trends')
            league_info['formtable'] = league_info['latest'].replace('latest','formtable')
            league_info['halftime'] = league_info['latest'].replace('latest','halftime')
            league_info['relative_form'] = league_info['latest'].replace('latest','table')+'&tid=re'
            league_info['relative_perf'] = league_info['latest'].replace('latest','table')+'&tid=rp'
            league_info['home_advantage'] = league_info['latest'].replace('latest','table')+'&tid=ha'
            league_info['points_win_lost'] = league_info['latest'].replace('latest','table')+'&tid=pw'
            league_info['current_streaks'] = league_info['latest'].replace('latest','table')+'&tid=g'
            league_info['goals_per_10'] = league_info['latest'].replace('latest','table')+'&tid=j'
            league_info['goals_per_15'] = league_info['latest'].replace('latest','table')+'&tid=k'
            league_info['over_under'] = league_info['latest'].replace('latest','table')+'&tid=c'
            league_info['lead_durations'] = league_info['latest'].replace('latest','table')+'&tid=t'
            league_info['goals_types'] = league_info['latest'].replace('latest','table')+'&tid=u'
            league_info['goals_ranges'] = league_info['latest'].replace('latest','table')+'&tid=9'
            league_info['abbr'] = add_on['text'] 
            leagues_infos.append(league_info)
        return leagues_infos
    
    # Relative Form
    def buildRelativeForm(self, url):
        page_relative_form = soup(get(url).content, "html.parser")
        td: Any = self.getElementByTagAndText(page_relative_form, 'td', 'Relative Form')
        data = td.parent.parent.find_all('tr',{"class": "odd"})
        relative_form_table = []
        for team_i_row in data:
            td_all = team_i_row.find_all("td")
            relative_form_team = {
                    'id':unicodedata.normalize("NFKD", td_all[1].get_text()).strip(),
                    'data': {
                    'GP': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text())),
                    'Pts': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text())),
                    'ppg_last8':self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text())),
                    'ppg_all':self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text())),
                    'rel_form': self.normalizePercent(unicodedata.normalize("NFKD", td_all[7].get_text()))
                    }
            }
            relative_form_table.append(relative_form_team)
        return relative_form_table
    
    # Relative Form
    def buildRelativePerformance(self, url):
        page_relative_performance = soup(get(url).content, "html.parser")
        data = page_relative_performance.find_all('table',{"id": "btable"})[0].find_all('tr',{"class": "odd"})
        relative_performance_table = []
        for team_i_row in data:
            ppg_team = 0
            rel_perf = 0
            td_all = team_i_row.find_all("td")
            try:
                 ppg_team = self.normalizeNum(unicodedata.normalize("NFKD", td_all[4].get_text()))
                 rel_perf =  self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text()))
            except:
                 ppg_team = 0
                 rel_perf = 0
            if ppg_team == 0:
                 ppg_opponents = 0
            else:
                 ppg_opponents =self.normalizeNum(float(rel_perf)/float(ppg_team))
            relative_performance_team = {
            'id':unicodedata.normalize("NFKD", td_all[1].get_text()).strip(),
            'data': {
            'GP': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text())),
            'Pts': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text())),
            'ppg_team':ppg_team,
            'ppg_opponents':ppg_opponents,
            'rel_perf': rel_perf
            }
            }
            relative_performance_table.append(relative_performance_team)
        return relative_performance_table
    
    # Points Win/Lost after 75th minutes
    def buildPointsWinAfter75(self, url):
         page_points = soup(get(url).content, "html.parser")
         data = page_points.find_all('table',{"id": "btable"})[0].find_all('tr',{"class": "odd"})
         points_win_after_75_table = []
         for team_i_row in data:
            td_all = team_i_row.find_all("td")
            pts_won_tag = td_all[4].find('a').find('font')
            pts_lost_tag = td_all[5].find('a').find('font')
            pts_won = self.normalizeNum(unicodedata.normalize("NFKD", pts_won_tag.get_text()))
            pts_lost = self.normalizeNum(unicodedata.normalize("NFKD", pts_lost_tag.get_text()))
            difference = self.normalizeNum(float(pts_won)-float(pts_lost))
            points_win_after_75_team = {
            'id':unicodedata.normalize("NFKD", td_all[1].get_text()).strip(),
            'data': {
            'GP': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text())),
            'Pts': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text())),
            'pts_won':pts_won,
            'pts_lost':pts_lost,
            'difference': difference
            }
            }
            points_win_after_75_table.append(points_win_after_75_team)     
         return points_win_after_75_table
    
    # Current Streaks
    def buildCurrentStreaks(self, url):
         page_current_streaks = soup(get(url).content, "html.parser")
         id = r"_Streaks"
         tab = self.get_tabs_by_id(page=page_current_streaks, id=id)
         data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
         current_streaks_table = []
         for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_tag = td_all[0].find('a')
                current_streaks_team = {
                    'id':unicodedata.normalize("NFKD", team_tag.get_text()).rstrip(),
                    'data': {
                    'w': self.normalizeNum(unicodedata.normalize("NFKD", td_all[1].get_text()).rstrip()),
                    'd': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text()).rstrip()),
                    'l': self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text()).rstrip()),
                    'no_w': self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text()).rstrip()),
                    'no_d': self.normalizeNum(unicodedata.normalize("NFKD", td_all[6].get_text()).rstrip()),
                    'no_l': self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text()).rstrip()),
                    'no_gf': self.normalizeNum(unicodedata.normalize("NFKD", td_all[8].get_text()).rstrip()),
                    'no_ga': self.normalizeNum(unicodedata.normalize("NFKD", td_all[9].get_text()).rstrip()),
                    }
                }
                current_streaks_table.append(current_streaks_team)
         return current_streaks_table
    
    # Over / Under (total goals)
    def buildOverUnderFullTime(self, url):
         page = soup(get(url).content, "html.parser")
         id = r"_TMG"
         tab = self.get_tabs_by_id(page=page, id=id)
         data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
         over_under_table = []
         for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                    'id': '',
                    'data': {
                    'GP': 0,
                    'AVG': 0.0,
                    'plus_0_5': 0,
                    'plus_1_5': 0,
                    'plus_2_5': 0,
                    'plus_3_5': 0,
                    'plus_4_5': 0,
                    'plus_5_5': 0,
                    'BTS': 0,
                    'CS': 0,
                    'FTS': 0,
                    'WTN': 0,
                    'LTN': 0
                    }
                }
                nameTeam = unicodedata.normalize("NFKD", td_all[0].get_text().strip())
                GP = unicodedata.normalize("NFKD", td_all[1].get_text().strip())
                AVG = unicodedata.normalize("NFKD", td_all[2].get_text().strip())
                plus_0_5 = unicodedata.normalize("NFKD", td_all[3].get_text().strip())
                plus_0_5 = float(plus_0_5.rstrip(plus_0_5[-1]))/100
                plus_1_5 = unicodedata.normalize("NFKD", td_all[4].get_text().strip())
                plus_1_5 = float(plus_1_5.rstrip(plus_1_5[-1]))/100
                plus_2_5 = unicodedata.normalize("NFKD", td_all[5].get_text().strip())
                plus_2_5 = float(plus_2_5.rstrip(plus_2_5[-1]))/100
                plus_3_5 = unicodedata.normalize("NFKD", td_all[6].get_text().strip())
                plus_3_5 = float(plus_3_5.rstrip(plus_3_5[-1]))/100
                plus_4_5 = unicodedata.normalize("NFKD", td_all[7].get_text().strip())
                plus_4_5 = float(plus_4_5.rstrip(plus_4_5[-1]))/100
                plus_5_5 = unicodedata.normalize("NFKD", td_all[8].get_text().strip())
                plus_5_5 = float(plus_5_5.rstrip(plus_5_5[-1]))/100
                BTS = unicodedata.normalize("NFKD", td_all[9].get_text().strip())
                BTS = float(BTS.rstrip(BTS[-1]))/100
                CS = unicodedata.normalize("NFKD", td_all[10].get_text().strip())
                CS = float(CS.rstrip(CS[-1]))/100
                FTS = unicodedata.normalize("NFKD", td_all[11].get_text().strip())
                FTS = float(FTS.rstrip(FTS[-1]))/100
                WTN = unicodedata.normalize("NFKD", td_all[12].get_text().strip())
                WTN = float(WTN.rstrip(WTN[-1]))/100
                LTN = unicodedata.normalize("NFKD", td_all[13].get_text().strip())
                LTN = float(LTN.rstrip(LTN[-1]))/100
                team_data['id'] = nameTeam.rstrip()
                team_data['data']['GP'] = int(GP)
                team_data['data']['AVG'] = float(AVG)
                team_data['data']['plus_0_5'] = plus_0_5 
                team_data['data']['plus_1_5'] = plus_1_5
                team_data['data']['plus_2_5'] = plus_2_5
                team_data['data']['plus_3_5'] = plus_3_5
                team_data['data']['plus_4_5'] = plus_4_5
                team_data['data']['plus_5_5'] = plus_5_5
                team_data['data']['BTS'] = BTS
                team_data['data']['CS'] = CS
                team_data['data']['FTS'] = FTS
                team_data['data']['WTN'] = WTN
                team_data['data']['LTN'] = LTN  
                over_under_table.append(team_data)
         return over_under_table

    # Lead durations
    def buildLeadDurations(self, url):
         page = soup(get(url).content, "html.parser")
         id = r"_SMIN_1"
         tab = self.get_tabs_by_id(page=page, id=id)
         data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
         lead_durations_table = []
         for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                     'id':unicodedata.normalize("NFKD", td_all[0].get_text()).rstrip(),
                     'data': {
                     'GP': self.normalizeNum(unicodedata.normalize("NFKD", td_all[1].get_text()).rstrip()),
                     'leading':self.normalizePercent(unicodedata.normalize("NFKD", td_all[3].get_text()).rstrip()),
                     'level':self.normalizePercent(unicodedata.normalize("NFKD", td_all[4].get_text()).rstrip()),
                     'trailing':self.normalizePercent(unicodedata.normalize("NFKD", td_all[5].get_text()).rstrip())
                     }
                }
                lead_durations_table.append(team_data)
         return lead_durations_table 
    # Goals types
    def buildGoalsTypes(self, url):
         page = soup(get(url).content, "html.parser")
         id = r"_SO"
         tab = self.get_tabs_by_id(page=page, id=id)
         data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
         goals_types_table = []
         for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                     'id':unicodedata.normalize("NFKD", td_all[0].get_text()).rstrip(),
                     'data': {
                     'GP': self.normalizeNum(unicodedata.normalize("NFKD", td_all[1].get_text()).rstrip()),
                     'team_goals_for_equalize':self.normalizeNum(unicodedata.normalize("NFKD", td_all[3].get_text()).rstrip()),
                     'team_goals_for_equalize_percent':self.normalizePercent(unicodedata.normalize("NFKD", td_all[4].get_text()).rstrip()),
                     'team_goals_for_take_lead':self.normalizeNum(unicodedata.normalize("NFKD", td_all[5].get_text()).rstrip()),
                     'team_goals_for_take_lead_percent':self.normalizePercent(unicodedata.normalize("NFKD", td_all[6].get_text()).rstrip()),
                     'team_goals_for_increase_lead':self.normalizeNum(unicodedata.normalize("NFKD", td_all[7].get_text()).rstrip()),
                     'team_goals_for_increase_lead_percent':self.normalizePercent(unicodedata.normalize("NFKD", td_all[8].get_text()).rstrip()),
                     'opponents_goals_for_equalize':self.normalizeNum(unicodedata.normalize("NFKD", td_all[9].get_text()).rstrip()),
                     'opponents_goals_for_equalize_percent':self.normalizePercent(unicodedata.normalize("NFKD", td_all[10].get_text()).rstrip()),
                     'opponents_goals_for_take_lead':self.normalizeNum(unicodedata.normalize("NFKD", td_all[11].get_text()).rstrip()),
                     'opponents_goals_for_take_lead_percent':self.normalizePercent(unicodedata.normalize("NFKD", td_all[12].get_text()).rstrip()),
                     'opponents_goals_for_increase_lead':self.normalizeNum(unicodedata.normalize("NFKD", td_all[13].get_text()).rstrip()),
                     'opponents_goals_for_increase_lead_percent':self.normalizePercent(unicodedata.normalize("NFKD", td_all[14].get_text()).rstrip()),
                     }
                }
                goals_types_table.append(team_data)
         return goals_types_table 
         

    # Goals ranges
    def buildGoalsRanges(self, url):
         page = soup(get(url).content, "html.parser")
         id = r"_TG2"
         tab = self.get_tabs_by_id(page=page, id=id)
         data = tab.find_all("table")[0].find_all('tr',{"class": "odd"})
         goals_ranges_table = []
         for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                     'id':unicodedata.normalize("NFKD", td_all[0].get_text().strip()).rstrip(),
                     'data': {
                     'GP': self.normalizeNum(unicodedata.normalize("NFKD", td_all[1].get_text().strip()).rstrip()),
                     'average': self.normalizeNum(unicodedata.normalize("NFKD", td_all[2].get_text().strip()).rstrip()),
                     'goals_0_1': self.normalizePercent(unicodedata.normalize("NFKD", td_all[3].get_text().strip()).rstrip()),
                     'goals_2_3': self.normalizePercent(unicodedata.normalize("NFKD", td_all[4].get_text().strip()).rstrip()),
                     'goals_4_plus': self.normalizePercent(unicodedata.normalize("NFKD", td_all[5].get_text().strip()).rstrip())
                     }
                }
                goals_ranges_table.append(team_data)
         return goals_ranges_table

    
    # Goals per 10 mn
    def buildGoalsPer10(self, url):
         page = soup(get(url).content, "html.parser")
         id = r"_GTS10"
         tab = self.get_tabs_by_id(page=page, id=id)
         data = tab.find_all("table")[1].find_all('tr',{"class": "trow8"})
         goals_per10_table = []
         for team_i_row in data:
                td_all = team_i_row.find_all("td")
                team_data = {
                     'id': unicodedata.normalize("NFKD", td_all[0].get_text().strip()).rstrip(),
                     'data': {
                     'time_0_10': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[1].get_text().strip()).rstrip()),
                     'time_11_20': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[2].get_text().strip()).rstrip()),
                     'time_21_30': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[3].get_text().strip()).rstrip()),
                     'time_31_40': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[4].get_text().strip()).rstrip()),
                     'time_41_50': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[5].get_text().strip()).rstrip()),
                     'time_51_60': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[6].get_text().strip()).rstrip()),
                     'time_61_70': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[7].get_text().strip()).rstrip()),
                     'time_71_80': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[8].get_text().strip()).rstrip()),
                     'time_81_90': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[9].get_text().strip()).rstrip()),
                     'first_half': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[11].get_text().strip()).rstrip()),
                     'second_half': self.getGoalsPerTime(unicodedata.normalize("NFKD", td_all[12].get_text().strip()).rstrip())      
                     }
                }
                goals_per10_table.append(team_data)
         return goals_per10_table   
                 
    # Debug Table
    def debugTable(self, url):
         page_debug = soup(get(url).content, "html.parser")
         tables = page_debug.find_all('table',{"id": "btable"})
         print(len(tables))
         for table in tables:
              print(table)
              

    # Utilitaires

    def isfloat(self,num):
         try:
              float(num)
              return True
         except ValueError:
            return False
    def normalizeNum(self, num):
        if num == '-' or num == '' or self.isfloat(num) == False:
            return 0
        else:
            return round(float(num),2)
        
    def normalizePercent(self, num):
        num = float(num.rstrip(num[-1]))/100
        return round(num,2)
    
    def getElementByTagAndText(self, page, tag, text) -> Tag:
        result: Any = None
        for t in page.find_all(tag):
            if t.text == text:
                result = t
        return result
    
    def getGoalsForAgainst(self,input):
         goals = {'GF': int(input.split()[0].rstrip()), 'GA': int(input.split()[2].rstrip())}
         return goals
    
    def getGoalsPerTime(self,input):
         goals = {'GF': int(input.split('-')[0].rstrip()), 'GA': int(input.split('-')[1].rstrip())}
         return goals
    
    def buildDoc(self, id, data):
         modelDocument = {}
         modelDocument['_id'] = id
         modelDocument["data"] = data
         modelDocument['date'] =  datetime.now().strftime('%Y-%m-%d %H:%M:%S')
         return modelDocument
    
    def remove_html(self,data):
        regex = re.compile(r'<[^>]+>')
        return regex.sub('', data)
    
    




        