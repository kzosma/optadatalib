import os
from ibmcloudant.cloudant_v1 import CloudantV1
class Cloudant:

    global _client
    
    def __init__(self, SERVICE_NAME_CLOUDANT, CLOUDANT_URL, CLOUDANT_APIKEY):
        os.environ['CLOUDANT_URL'] = CLOUDANT_URL
        os.environ['CLOUDANT_APIKEY'] = CLOUDANT_APIKEY
        os.environ['SERVICE_NAME_CLOUDANT'] = SERVICE_NAME_CLOUDANT
    def getClient(self):
        if not '_client' in globals():
            _client = CloudantV1.new_instance(service_name=os.environ.get('SERVICE_NAME_CLOUDANT', ''))
        return _client
    def getAllDataFromDB(self, db_name):
        allData = self.getClient().post_all_docs(
                db=db_name,
                include_docs=bool(True)
                ).get_result()['rows']
        return allData
    
    
