import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
from optadatalib.Helper import Utils
from collections import namedtuple 

class OptaAi:

    def getPythagoreanExpectation(data):
        df_cloudant = pd.json_normalize(data)
        df_cloudant_data = []
        for fixtures in df_cloudant['doc.fixtures']:
            for fixture in fixtures:
                fixture_data = {}
                fixture_data['idMatch'] = fixture['idMatch']
                fixture_data['date'] = fixture['date']
                fixture_data['HomeTeam'] = np.array2string(np.where(fixture['h_a']=='h', fixture['name_for'],fixture['name_against']))
                fixture_data['AwayTeam'] = np.array2string(np.where(fixture['h_a']=='a', fixture['name_for'],fixture['name_against']))
                fixture_data['HomeGoal'] = np.where(fixture['h_a']=='h', fixture['g_for'],fixture['g_against']).sum()
                fixture_data['AwayGoal'] = np.where(fixture['h_a']=='a', fixture['g_for'],fixture['g_against']).sum()
                df_cloudant_data.append(fixture_data)
        df_cloudant_data_8 = pd.json_normalize(Utils.remove_duplicates_by_property(df_cloudant_data, 'idMatch'))
        df_cloudant_data_8['hwin'] = np.where(df_cloudant_data_8['HomeGoal'] > df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['awin'] = np.where(df_cloudant_data_8['HomeGoal'] < df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['count'] = 1
        df_cloudant_data_8_home = df_cloudant_data_8.groupby('HomeTeam')[['hwin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_home = df_cloudant_data_8_home.rename(columns={'HomeTeam':'team','HomeGoal':'HomeGH', 'AwayGoal':'HomeGA', 'count':'MH'})
        df_cloudant_data_8_away = df_cloudant_data_8.groupby('AwayTeam')[['awin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_away = df_cloudant_data_8_away.rename(columns={'AwayTeam':'team','HomeGoal':'AwayGH', 'AwayGoal':'AwayGA', 'count':'MA'})
        df_cloudant_data_8 = pd.merge(df_cloudant_data_8_home, df_cloudant_data_8_away,on='team')
        df_cloudant_data_8['M'] = df_cloudant_data_8['MH'] + df_cloudant_data_8['MA']
        df_cloudant_data_8['W'] = df_cloudant_data_8['hwin'] + df_cloudant_data_8['awin']
        df_cloudant_data_8['GF'] = df_cloudant_data_8['HomeGH'] + df_cloudant_data_8['AwayGA']
        df_cloudant_data_8['GA'] = df_cloudant_data_8['HomeGA'] + df_cloudant_data_8['AwayGH']
        df_cloudant_data_8['wpc'] = df_cloudant_data_8['W'] / df_cloudant_data_8['M']
        df_cloudant_data_8['pyth'] = df_cloudant_data_8['GF']**2 / (df_cloudant_data_8['GF']**2 + df_cloudant_data_8['GA']**2)
        pyth_lm = smf.ols(formula='wpc ~ pyth', data=df_cloudant_data_8).fit()
        df = pd.read_html(pyth_lm.summary().tables[1].as_html(),header=0,index_col=0)[0]
        pyth=df['coef'].values[1]
        intercept = df['coef'][0]
        confidence = df['P>|t|'][1]
        PythExpectation = namedtuple('PythExpectation', 'pyth intercept confidence')
        pe = PythExpectation(pyth=pyth, intercept=intercept, confidence=confidence)
        return pe
    def getPythagoreanStat(team):
        df_cloudant = pd.json_normalize(team)
        df_cloudant_data = []
        for fixtures in df_cloudant['doc.fixtures']:
            for fixture in fixtures:
                fixture_data = {}
                fixture_data['idMatch'] = fixture['idMatch']
                fixture_data['date'] = fixture['date']
                fixture_data['HomeTeam'] = np.array2string(np.where(fixture['h_a']=='h', fixture['name_for'],fixture['name_against']))
                fixture_data['AwayTeam'] = np.array2string(np.where(fixture['h_a']=='a', fixture['name_for'],fixture['name_against']))
                fixture_data['HomeGoal'] = np.where(fixture['h_a']=='h', fixture['g_for'],fixture['g_against']).sum()
                fixture_data['AwayGoal'] = np.where(fixture['h_a']=='a', fixture['g_for'],fixture['g_against']).sum()
                df_cloudant_data.append(fixture_data)
        df_cloudant_data_8 = pd.json_normalize(df_cloudant_data)
        df_cloudant_data_8['hwin'] = np.where(df_cloudant_data_8['HomeGoal'] > df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['awin'] = np.where(df_cloudant_data_8['HomeGoal'] < df_cloudant_data_8['AwayGoal'],1,0)
        df_cloudant_data_8['count'] = 1
        df_cloudant_data_8_home = df_cloudant_data_8.groupby('HomeTeam')[['hwin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_team_home = df_cloudant_data_8_home[df_cloudant_data_8_home['count'] > 1] 
        df_cloudant_data_8_team_home = df_cloudant_data_8_team_home.rename(columns={'HomeTeam':'team','HomeGoal':'HomeGH', 'AwayGoal':'HomeGA', 'count':'MH'})
        df_cloudant_data_8_away = df_cloudant_data_8.groupby('AwayTeam')[['awin','HomeGoal','AwayGoal','count']].sum().reset_index()
        df_cloudant_data_8_team_away = df_cloudant_data_8_away[df_cloudant_data_8_away['count'] > 1] 
        df_cloudant_data_8_team_away = df_cloudant_data_8_team_away.rename(columns={'AwayTeam':'team','HomeGoal':'AwayGH', 'AwayGoal':'AwayGA', 'count':'MA'})
        df_cloudant_data_8 = pd.merge(df_cloudant_data_8_team_home, df_cloudant_data_8_team_away,on='team')
        df_cloudant_data_8['M'] = df_cloudant_data_8['MH'] + df_cloudant_data_8['MA']
        df_cloudant_data_8['W'] = df_cloudant_data_8['hwin'] + df_cloudant_data_8['awin']
        df_cloudant_data_8['GF'] = df_cloudant_data_8['HomeGH'] + df_cloudant_data_8['AwayGA']
        df_cloudant_data_8['GA'] = df_cloudant_data_8['HomeGA'] + df_cloudant_data_8['AwayGH']
        df_cloudant_data_8['wpc'] = df_cloudant_data_8['W'] / df_cloudant_data_8['M']
        df_cloudant_data_8['pyth'] = df_cloudant_data_8['GF']**2 / (df_cloudant_data_8['GF']**2 + df_cloudant_data_8['GA']**2)
        M=df_cloudant_data_8['M']   
        W=df_cloudant_data_8['W']
        GF=df_cloudant_data_8['GF']
        GA=df_cloudant_data_8['GA']
        wpc=df_cloudant_data_8['wpc']
        pyth=df_cloudant_data_8['pyth']
        PythExpectation = namedtuple('PythExpectation', 'M W GF GA wpc pyth')
        pe = PythExpectation(M=M, W=W, GF=GF, GA=GA, wpc=wpc, pyth=pyth)
        return pe



        
    
