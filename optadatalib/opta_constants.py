# Information Cloudant Database
dbname_teams = 'optadb_teams'
dbname_players = 'optadb_players'
dbname_gps = 'optadb_gps'
dbname_ionic = 'optadb_ionic'
dbname_soccer_stats = 'soccer_stats'


# Liste des saisons
seasons = ['2022']
# Liste des leagues
leagues = ['La_liga', 'EPL', 'Bundesliga', 'Serie_A', 'Ligue_1']
LIGUE1 = 'Ligue_1'
LIGA = 'La_liga'
SERIEA = 'Serie_A'
BUNDESLIGA = 'Bundesliga'
EPL = 'EPL'

# Liste de modèles base de données
model_team_whoscored = 'model_team_whoscored'
model_team_classements = 'model_team_classements'

# NBA
# Variables
nba_season = 2023
# DBS
dbname_nba_per_game = 'optadb_nba_per_game'
dbname_nba_totals = 'optadb_nba_totals'
dbname_nba_shooting = 'optadb_nba_shooting'
dbname_nba_advanced = 'optadb_nba_advanced'
dbname_nba_per_poss = 'optadb_nba_per_poss'

# Soccer Stat
url_soccer_stat = "https://www.soccerstats.com"
soccer_stats_leagues = ['AT','AU','AR','BE','BR','CH','CHI','COL','CRO','CZR','DE','D2','ECU','EN','E2','ES','ES2','FI','FR','F2','GR','IRE','ISR','JP','KR','MEX', 'NL','NOI','NOR', 'IT','PAR', 'PL','PT','ROM','SC','SE','SLO','TR', 'USA', 'WAL']

