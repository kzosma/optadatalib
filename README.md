# OptaDataLib

## Lancement des tests unitaires globaux
> python -m unittest discover tests

## PyTest
> python setup.py pytest

## Build library
> python setup.py bdist_wheel

## Install
> pip install --force-reinstall ./dist/optadatalib-0.1.0-py3-none-any.whl

## Delete folder build and dist
> rmdir build /s /q
> rmdir dist /s /q

## Documentation

https://medium.com/analytics-vidhya/how-to-create-a-python-library-7d5aea80cc3f


# Convert JSON to Panda DataFrame
https://towardsdatascience.com/how-to-convert-json-into-a-pandas-dataframe-100b2ae1e0d8



