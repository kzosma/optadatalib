import unittest
from optadatalib.Helper import Utils
from optadatalib import opta_constants
class TestHelper(unittest.TestCase):
  # First Test
  def test_constant(self):
      self.assertEqual(opta_constants.dbname_ionic, 'optadb_ionic')
  # Second Test
  def test_ComputeDistanceHarvesine(self):
    homeGPS = {'latitude': 45.7597, 'longitude': 4.8422}
    awayGPS = {'latitude': 48.8567, 'longitude': 2.3508}
    self.assertEqual(Utils.computeDistanceHarvesine(
      self,
      homeGPS=homeGPS,
      awayGPS=awayGPS), 392.2172595594006)

if __name__ == '__main__':
    unittest.main()