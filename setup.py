from setuptools import find_packages, setup
setup(
    name='optadatalib',
    packages=find_packages(),
    version='0.1.0',
    description='Librairie Opta Data',
    url='https://gitlab.com/kzosma/optadatalib.git',
    author='optadatasoccer',
    author_email='optadatasoccer@gmail.com',
    license='MIT',
    install_requires=['beautifulsoup4==4.11.1','retry==0.9.2','ibmcloudant==0.1.3','haversine==2.7.0','python-dotenv==0.20.0','numpy==1.23.3','lxml==4.9.1','pandas==1.5.1'],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==7.2.0'],
    test_suite='tests'
    )